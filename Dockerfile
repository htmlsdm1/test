FROM nginx
COPY index.html /usr/share/nginx/html/
COPY Second.html /usr/share/nginx/html/
COPY Style.css /usr/share/nginx/html/
COPY Validation.js /usr/share/nginx/html/
EXPOSE 80